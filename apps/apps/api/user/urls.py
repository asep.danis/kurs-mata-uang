from django.conf.urls import url
from apps.api.user.views import UserRegistrationView
from apps.api.user.views import UserLoginView

urlpatterns = [
    url(r'^signup', UserRegistrationView.as_view()),
    url(r'^login', UserLoginView.as_view()),
]