from django.conf.urls import url
from apps.api.userProfile.views import UserProfileView

urlpatterns = [
    url(r'^profile', UserProfileView.as_view()),
]