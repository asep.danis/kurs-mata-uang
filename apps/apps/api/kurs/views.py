from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from apps.api.kurs.serializers import KursSerializer
from apps.api.kurs.models import Kurs

# Create your views here.
class KursView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication
    serializer_class = KursSerializer
    queryset = Kurs.objects.all()