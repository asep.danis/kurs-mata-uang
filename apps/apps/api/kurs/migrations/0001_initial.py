# Generated by Django 3.0.7 on 2020-06-27 17:11

from django.db import migrations, models
import django.utils.timezone
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Kurs',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('no', models.CharField(max_length=50, unique=True)),
                ('currency', models.CharField(max_length=50)),
                ('value', models.CharField(max_length=50)),
                ('shift', models.CharField(max_length=50)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
                'db_table': 'kurs',
                'ordering': ['no'],
            },
        ),
    ]
