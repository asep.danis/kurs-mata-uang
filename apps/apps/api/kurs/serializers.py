from rest_framework import serializers
from apps.api.kurs.models import Kurs

class KursSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Kurs
        fields = ('no', 'id', 'currency', 'value', 'shift')