import ssl
import json
from bs4 import BeautifulSoup
from urllib.request import urlopen
from django.core.management.base import BaseCommand
from apps.api.kurs.models import Kurs

class Command(BaseCommand):
    help = "collect kurs"

    def handle(self, *args, **options):
        context = ssl._create_unverified_context()
        html = urlopen('https://fiskal.kemenkeu.go.id/informasi-publik/kurs-pajak', context=context)
        soup = BeautifulSoup(html, 'html.parser')
        tbody = soup.find('tbody')
        data = tbody.find_all("tr")
        for result in data:
            no = result.find('td', class_='text-center').text
            currency = result.find('td', class_='text-left').text
            value = result.find('div', class_='ml-5').text
            shift = result.select('.text-right.curr-change')[-1].text
            try:
                Kurs.objects.create(
                    no=no,
                    currency=currency,
                    value=value,
                    shift=shift
                )
                print('%s %s %s %s' % (no, currency, value, shift,))
            except:
                Kurs.objects.all().delete()
                print('%s %s %s %s already exists' % (no, currency, value, shift,))
                Kurs.objects.create(
                    no=no,
                    currency=currency,
                    value=value,
                    shift=shift
                )
        self.stdout.write( 'kurs complete' )