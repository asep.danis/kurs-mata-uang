from django.contrib import admin
from apps.api.kurs.models import Kurs

# Register your models here.
admin.site.register(Kurs)
