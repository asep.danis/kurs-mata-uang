import uuid
from django.db import models
from django.utils import timezone

# Create your models here.
class Kurs(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    no = models.IntegerField(unique=True)
    currency = models.CharField(max_length=50, unique=False)
    value = models.CharField(max_length=50, unique=False)
    shift = models.CharField(max_length=50, unique=False)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.currency
    
    class Meta:
        ordering = ['no']
        db_table = "kurs"
    
    class Admin:
        pass
    