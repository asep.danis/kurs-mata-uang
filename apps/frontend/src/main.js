import Vue from 'vue'
import axios from 'axios'
import router from './router'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  el: '#app',
  beforeCreate () {
    Vue.prototype.$http = axios
    axios.defaults.xsrfHeaderName = 'X-CSRFToken'
    axios.defaults.xsrfCookieName = 'csrftoken'
  },
  router,
  template: '<App/>',
  components: { App }
  // render: h => h(App)
})
