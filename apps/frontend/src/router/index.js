import VueRouter from 'vue-router'
import Vue from 'vue'
import Login from '../components/Login'
import Kurs from '../components/Kurs'
import KursPage from '../components/KursPage'

Vue.use(VueRouter)

const router = new VueRouter({
    routes: [
      {
        path: '/login',
        name: 'Login',
        component: Login
      },
      {
        path: '/kurs',
        name: 'Kurs',
        component: Kurs
      },
      {
        path: '/kurs/:id',
        name: 'KursPage',
        component: KursPage
      }
    ]
  })
  
  router.beforeEach((to, from, next) => {
    if (sessionStorage.getItem('token') !== null || to.path === '/login') {
      next()
    } else {
      next('/login')
    }
  })
  
  export default router
