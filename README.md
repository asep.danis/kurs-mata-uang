# kurs-mata-uang

## Running the code

### Django

To get the Django server running:

Install the requirements from pip

```bash
pip install -r requirements.txt
```

Run Get data from https://fiskal.kemenkeu.go.id/informasi-publik/kurs-pajak

```bash
python manage.py scrape
```

Run django's development server (starts on localhost:8000):

```bash
python manage.py runserver
```

### Vue

Navigate to the `frontend directory`:

```bash
cd frontend
```

Install the dependencies from npm:

``` bash
npm install
```

Run the webpack dev server (starts on localhost:8080):

```bash
npm run dev
```